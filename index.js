const
    axios = require('axios'),
    userAgents = require('user-agents');

module.exports = ({
    domain = 'invidious.snopyta.org',
    axiosAdapter
} = {}) => {
    const
        rootUrl = `https://${domain}`,
        _axios = axios.create({
            baseURL: `${rootUrl}/api/v1`,
            adapter: axiosAdapter
        });
    _axios.defaults.headers.common['User-Agent'] = userAgents.random().toString();
    return {
        instance: { getInfo: async () => (await _axios('stats')).data },
        search: async ({
            query,
            page,
            sortBy,
            date,
            duration,
            type,
            features = [],
            region
        }) => (await _axios('search', {
            params: {
                'q': query,
                page,
                'sort_by': sortBy,
                date,
                duration,
                type,
                features: features.join(','),
                region
            }
        })).data,
        channel: ({ channelId }) => ({
            getInfo: async () => (await _axios(`channels/${channelId}`)).data,
            getVideos: async ({
                page,
                sortBy
            } = {}) => (await _axios(`channels/${channelId}/videos`, {
                params: {
                    page,
                    'sort_by': sortBy
                }
            })).data,
            getPlaylists: async () => (await _axios(`channels/playlists/${channelId}`)).data,
            getComments: async () => (await _axios(`channels/comments/${channelId}`)).data,
            search: async ({
                query,
                page
            }) => (await _axios(`channels/search/${channelId}`, {
                params: {
                    'q': query,
                    page
                }
            })).data
        }),
        video: ({ videoId }) => ({
            getInfo: async () => (await _axios(`videos/${videoId}`)).data,
            getComments: async ({
                sortBy,
                continuation
            } = {}) => (await _axios(`comments/${videoId}`, {
                params: {
                    'sort_by': sortBy,
                    continuation
                }
            })).data,
            getCaptions: async () => (await _axios(`captions/${videoId}`)).data['captions'].map(caption => ({
                languageCode: caption['languageCode'],
                label: caption['label']
            })),
            getCaption: async ({ captionLabel }) => (await _axios(`captions/${videoId}`, { params: { label: captionLabel } })).data
        }),
        getPlaylist: async ({
            playlistId,
            page
        }) => (await _axios(`playlists/${playlistId}`, { params: { page } })).data,
        getMix: async ({ mixId }) => (await _axios(`mixes/${mixId}`)).data,
        getTrending: async () => (await _axios('trending')).data,
        getPopular: async () => (await _axios('popular')).data
    };
};